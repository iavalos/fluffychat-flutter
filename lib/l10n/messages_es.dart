// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  static m0(username) => "${username} aceptó la invitación";

  static m1(username) => "${username} activó encripción extremo-a-extremo";

  static m2(username, targetName) => "${username} prohibió a ${targetName}";

  static m3(homeserver) => "Por defecto, estarás conectadx a ${homeserver}";

  static m4(username) => "${username} cambió el ávatar del chat";

  static m5(username, description) => "${username} cambió la descripción del chat a: \'${description}\'";

  static m6(username, chatname) => "${username} cambió el nombre del chat a: \'${chatname}\'";

  static m7(username) => "${username} cambió los permisos del chat";

  static m8(username, displayname) => "${username} cambió su nombre de pantalla a: ${displayname}";

  static m9(username) => "${username} cambió las reglas de acceso para invitados";

  static m10(username, rules) => "${username} cambió las reglas de acceso para invitados a: ${rules}";

  static m11(username) => "${username} cambió la visibilidad del historial";

  static m12(username, rules) => "${username} cambió la visibilidad del historial a: ${rules}";

  static m13(username) => "${username} cambió las reglas para unirse";

  static m14(username, joinRules) => "${username} cambió las reglas para unirse a: ${joinRules}";

  static m15(username) => "${username} cambió su ávatar de perfil";

  static m16(username) => "${username} cambió los apodos de la sala";

  static m17(username) => "${username} cambió el link de invitación";

  static m18(error) => "No se pudo desencriptar el mensaje: ${error}";

  static m19(count) => "${count} participantes";

  static m20(username) => "${username} creó el chat";

  static m21(date, timeOfDay) => "${date}, ${timeOfDay}";

  static m22(year, month, day) => "${year}-${month}-${day}";

  static m23(month, day) => "${month}-${day}";

  static m24(displayname) => "Grupo con ${displayname}";

  static m25(username, targetName) => "${username} ha retirado la invitación para ${targetName}";

  static m26(groupName) => "Invitar contacto a ${groupName}";

  static m27(username, link) => "${username} te invitó a FluffyChat. \n1. Instala FluffyChat: http://fluffy.chat \n2. Regístrate o inicia sesión \n3. Abre el link de la invitación: ${link}";

  static m28(username, targetName) => "${username} invitó a ${targetName}";

  static m29(username) => "${username} se unió al chat";

  static m30(username, targetName) => "${username} sacó a ${targetName}";

  static m31(username, targetName) => "${username} sacó y prohibió a ${targetName}";

  static m32(localizedTimeShort) => "Activo: ${localizedTimeShort}";

  static m33(count) => "Cargar ${count} más participantes";

  static m34(homeserver) => "Iniciar sesión en ${homeserver}";

  static m35(number) => "${number} seleccionado";

  static m36(fileName) => "Reproducir ${fileName}";

  static m37(username) => "${username} redactó un evento";

  static m38(username) => "${username} rechazó la invitación";

  static m39(username) => "Eliminado por ${username}";

  static m40(username) => "Visto por ${username}";

  static m41(username, count) => "Vist por ${username} y ${count} otros";

  static m42(username, username2) => "Visto por ${username} y ${username2}";

  static m43(username) => "${username} envió un archivo";

  static m44(username) => "${username} envió una imagen";

  static m45(username) => "${username} envió un sticker";

  static m46(username) => "${username} envió un video";

  static m47(username) => "${username} envió un audio";

  static m48(username) => "${username} ha compartido la ubicación";

  static m49(hours12, hours24, minutes, suffix) => "${hours12}:${minutes} ${suffix}";

  static m50(username, targetName) => "${username} desprohibió ${targetName}";

  static m51(type) => "Evento desconocido \'${type}\'";

  static m52(unreadCount) => "${unreadCount} chats no leídos";

  static m53(unreadEvents) => "${unreadEvents} mensajes no leídos";

  static m54(unreadEvents, unreadChats) => "${unreadEvents} mensaje no leídos en ${unreadChats} chats";

  static m55(username, count) => "${username} y ${count} otros están escribiendo…";

  static m56(username, username2) => "${username} y ${username2} están escribiendo…";

  static m57(username) => "${username} está escribiendo…";

  static m58(username) => "${username} dejó el chat";

  static m59(username, type) => "${username} envió un evento ${type}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "(Optional) Group name" : MessageLookupByLibrary.simpleMessage("(Optional) Nombre del grupo"),
    "About" : MessageLookupByLibrary.simpleMessage("Acerca de"),
    "Account" : MessageLookupByLibrary.simpleMessage("Cuenta"),
    "Account informations" : MessageLookupByLibrary.simpleMessage("Información de cuenta"),
    "Add a group description" : MessageLookupByLibrary.simpleMessage("Añadir descripción al grupo"),
    "Admin" : MessageLookupByLibrary.simpleMessage("Admin"),
    "Already have an account?" : MessageLookupByLibrary.simpleMessage("¿Ya tienes una cuenta?"),
    "Anyone can join" : MessageLookupByLibrary.simpleMessage("Cualquiera puede unirse"),
    "Archive" : MessageLookupByLibrary.simpleMessage("Archivar"),
    "Archived Room" : MessageLookupByLibrary.simpleMessage("Sala archivada"),
    "Are guest users allowed to join" : MessageLookupByLibrary.simpleMessage("Se le permite entrar a invitados"),
    "Are you sure?" : MessageLookupByLibrary.simpleMessage("¿Estás segurx?"),
    "Authentication" : MessageLookupByLibrary.simpleMessage("Autenticación"),
    "Avatar has been changed" : MessageLookupByLibrary.simpleMessage("El ávatar ha sido cambiado"),
    "Ban from chat" : MessageLookupByLibrary.simpleMessage("Prohibir del chat"),
    "Banned" : MessageLookupByLibrary.simpleMessage("Prohibido"),
    "Cancel" : MessageLookupByLibrary.simpleMessage("Cancelar"),
    "Change the homeserver" : MessageLookupByLibrary.simpleMessage("Cambiar de servidor"),
    "Change the name of the group" : MessageLookupByLibrary.simpleMessage("Cambiar el nombre del grupo"),
    "Change the server" : MessageLookupByLibrary.simpleMessage("Cambiar el servidor"),
    "Change wallpaper" : MessageLookupByLibrary.simpleMessage("Cambiar fondo de pantalla"),
    "Change your style" : MessageLookupByLibrary.simpleMessage("Cambiar tu estilo"),
    "Changelog" : MessageLookupByLibrary.simpleMessage("Historial de cambios"),
    "Chat" : MessageLookupByLibrary.simpleMessage("Chat"),
    "Chat details" : MessageLookupByLibrary.simpleMessage("Detalles del chat"),
    "Choose a strong password" : MessageLookupByLibrary.simpleMessage("Escoja una contraseña fuerte"),
    "Choose a username" : MessageLookupByLibrary.simpleMessage("Escoja un nombre de usuario"),
    "Close" : MessageLookupByLibrary.simpleMessage("Cerrar"),
    "Confirm" : MessageLookupByLibrary.simpleMessage("Confirmar"),
    "Connect" : MessageLookupByLibrary.simpleMessage("Conectar"),
    "Connection attempt failed" : MessageLookupByLibrary.simpleMessage("Intento de conexión fallido"),
    "Contact has been invited to the group" : MessageLookupByLibrary.simpleMessage("El contacto ha sido invitado al grupo"),
    "Content viewer" : MessageLookupByLibrary.simpleMessage("Visor de contenido"),
    "Copied to clipboard" : MessageLookupByLibrary.simpleMessage("Copiado al portapapeles"),
    "Copy" : MessageLookupByLibrary.simpleMessage("Copiar"),
    "Could not set avatar" : MessageLookupByLibrary.simpleMessage("No se pudo asignar ávatar"),
    "Could not set displayname" : MessageLookupByLibrary.simpleMessage("No se pudo asignar nombre de pantalla"),
    "Create" : MessageLookupByLibrary.simpleMessage("Crear"),
    "Create account now" : MessageLookupByLibrary.simpleMessage("Crear cuenta ahora"),
    "Create new group" : MessageLookupByLibrary.simpleMessage("Crear nuevo grupo"),
    "Currenlty active" : MessageLookupByLibrary.simpleMessage("En línea"),
    "Dark" : MessageLookupByLibrary.simpleMessage("Obscuro"),
    "Delete" : MessageLookupByLibrary.simpleMessage("Eliminar"),
    "Delete message" : MessageLookupByLibrary.simpleMessage("Eliminar mensaje"),
    "Deny" : MessageLookupByLibrary.simpleMessage("Denegar"),
    "Device" : MessageLookupByLibrary.simpleMessage("Dispositivo"),
    "Devices" : MessageLookupByLibrary.simpleMessage("Dispositivos"),
    "Discard picture" : MessageLookupByLibrary.simpleMessage("Descartar imagen"),
    "Displayname has been changed" : MessageLookupByLibrary.simpleMessage("El nombre de pantalla ha sido cambiado"),
    "Donate" : MessageLookupByLibrary.simpleMessage("Donar"),
    "Download file" : MessageLookupByLibrary.simpleMessage("Descargar archivo"),
    "Edit Jitsi instance" : MessageLookupByLibrary.simpleMessage("Editar instancia de Jitsi"),
    "Edit displayname" : MessageLookupByLibrary.simpleMessage("Editar nombre de pantalla"),
    "Emote Settings" : MessageLookupByLibrary.simpleMessage("Ajustes de emoticones"),
    "Emote shortcode" : MessageLookupByLibrary.simpleMessage("Atajo de emoticones"),
    "Empty chat" : MessageLookupByLibrary.simpleMessage("Chat vacío"),
    "Encryption algorithm" : MessageLookupByLibrary.simpleMessage("Algoritmo de encripción"),
    "Encryption is not enabled" : MessageLookupByLibrary.simpleMessage("La encripción no está activada"),
    "End to end encryption is currently in Beta! Use at your own risk!" : MessageLookupByLibrary.simpleMessage("¡La encripción extremo-a-extremo está actualmente en beta! ¡Utiliza bajo tu propio riesgo!"),
    "End-to-end encryption settings" : MessageLookupByLibrary.simpleMessage("Configuración de encripción extremo-a-extremo"),
    "Enter a group name" : MessageLookupByLibrary.simpleMessage("Introduce un nombre de grupo"),
    "Enter a username" : MessageLookupByLibrary.simpleMessage("Introduce un nombre de usuario"),
    "Enter your homeserver" : MessageLookupByLibrary.simpleMessage("Introduce tu servidor"),
    "File name" : MessageLookupByLibrary.simpleMessage("Nombre del archivo"),
    "File size" : MessageLookupByLibrary.simpleMessage("Tamaño del archivo"),
    "FluffyChat" : MessageLookupByLibrary.simpleMessage("FluffyChat"),
    "Forward" : MessageLookupByLibrary.simpleMessage("Reenviar"),
    "Friday" : MessageLookupByLibrary.simpleMessage("Viernes"),
    "From joining" : MessageLookupByLibrary.simpleMessage("Desde unirse"),
    "From the invitation" : MessageLookupByLibrary.simpleMessage("Desde la invitación"),
    "Group" : MessageLookupByLibrary.simpleMessage("Grupo"),
    "Group description" : MessageLookupByLibrary.simpleMessage("Descripción del grupo"),
    "Group description has been changed" : MessageLookupByLibrary.simpleMessage("La descripción del grupo ha sido cambiada"),
    "Group is public" : MessageLookupByLibrary.simpleMessage("El grupo es público"),
    "Guests are forbidden" : MessageLookupByLibrary.simpleMessage("Los invitados están prohibidos"),
    "Guests can join" : MessageLookupByLibrary.simpleMessage("Los invitados pueden unirse"),
    "Help" : MessageLookupByLibrary.simpleMessage("Ayuda"),
    "Homeserver is not compatible" : MessageLookupByLibrary.simpleMessage("El servidor no es compatible"),
    "How are you today?" : MessageLookupByLibrary.simpleMessage("¿Cómo te sientes hoy?"),
    "ID" : MessageLookupByLibrary.simpleMessage("ID"),
    "Identity" : MessageLookupByLibrary.simpleMessage("Identidad"),
    "Invite contact" : MessageLookupByLibrary.simpleMessage("Invitar contacto"),
    "Invited" : MessageLookupByLibrary.simpleMessage("Invitado"),
    "Invited users only" : MessageLookupByLibrary.simpleMessage("Solo usuarios invitados"),
    "It seems that you have no google services on your phone. That\'s a good decision for your privacy! To receive push notifications in FluffyChat we recommend using microG: https://microg.org/" : MessageLookupByLibrary.simpleMessage("Parece que no tienes los servicios de Google en tu teléfono. ¡Es una buena decisión para tu privacidad! Para recibir notificaciones push en FluffyChat, recomendamos utilizar migroG: https://microg.org/"),
    "Kick from chat" : MessageLookupByLibrary.simpleMessage("Sacar del chat"),
    "Last seen IP" : MessageLookupByLibrary.simpleMessage("Última IP activa"),
    "Leave" : MessageLookupByLibrary.simpleMessage("Leave"),
    "Left the chat" : MessageLookupByLibrary.simpleMessage("Dejó el chat"),
    "License" : MessageLookupByLibrary.simpleMessage("Licencia"),
    "Light" : MessageLookupByLibrary.simpleMessage("Claro"),
    "Load more..." : MessageLookupByLibrary.simpleMessage("Cargar más…"),
    "Loading... Please wait" : MessageLookupByLibrary.simpleMessage("Cargando… por favor espera"),
    "Login" : MessageLookupByLibrary.simpleMessage("Iniciar sesión"),
    "Logout" : MessageLookupByLibrary.simpleMessage("Cerrar sesión"),
    "Make a moderator" : MessageLookupByLibrary.simpleMessage("Hacer moderador"),
    "Make an admin" : MessageLookupByLibrary.simpleMessage("Hacer admin"),
    "Make sure the identifier is valid" : MessageLookupByLibrary.simpleMessage("Asegúrate de que el identificador sea válido"),
    "Message will be removed for all participants" : MessageLookupByLibrary.simpleMessage("El mensaje será eliminado para todos los participantes"),
    "Moderator" : MessageLookupByLibrary.simpleMessage("Moderador"),
    "Monday" : MessageLookupByLibrary.simpleMessage("Lunes"),
    "Mute chat" : MessageLookupByLibrary.simpleMessage("Silenciar chat"),
    "New message in FluffyChat" : MessageLookupByLibrary.simpleMessage("Nuevo mensaje en FluffyChat"),
    "New private chat" : MessageLookupByLibrary.simpleMessage("Nuevo mensaje privado"),
    "No emotes found. 😕" : MessageLookupByLibrary.simpleMessage("No se encontraron emoticones.    "),
    "No permission" : MessageLookupByLibrary.simpleMessage("No tienes permiso"),
    "No rooms found..." : MessageLookupByLibrary.simpleMessage("No se encontraron salas…"),
    "None" : MessageLookupByLibrary.simpleMessage("Ninguno"),
    "Not supported in web" : MessageLookupByLibrary.simpleMessage("No soportado en web"),
    "Oops something went wrong..." : MessageLookupByLibrary.simpleMessage("Ups, algo salió mal…"),
    "Open app to read messages" : MessageLookupByLibrary.simpleMessage("Abrir aplicación para leer los mensajes"),
    "Open camera" : MessageLookupByLibrary.simpleMessage("Abrir cámara"),
    "Participating user devices" : MessageLookupByLibrary.simpleMessage("Dispositivos de usuarios participantes"),
    "Password" : MessageLookupByLibrary.simpleMessage("Contraseña"),
    "Pick image" : MessageLookupByLibrary.simpleMessage("Selecciona imagen"),
    "Please be aware that you need Pantalaimon to use end-to-end encryption for now." : MessageLookupByLibrary.simpleMessage("Por favor ten en cuenta que necesitas Pantalaimon para utilizar encripción extremo a extremo por ahora."),
    "Please choose a username" : MessageLookupByLibrary.simpleMessage("Por favor, escoje un nombre de usuario"),
    "Please enter a matrix identifier" : MessageLookupByLibrary.simpleMessage("Por favor, ingresa un identificador de Matrix"),
    "Please enter your password" : MessageLookupByLibrary.simpleMessage("Por favor, introduce tu contraseña"),
    "Please enter your username" : MessageLookupByLibrary.simpleMessage("Por favor, introduce tu nombre de usuario"),
    "Public Rooms" : MessageLookupByLibrary.simpleMessage("Salas públicas"),
    "Recording" : MessageLookupByLibrary.simpleMessage("Grabando"),
    "Rejoin" : MessageLookupByLibrary.simpleMessage("Volver a unir"),
    "Remove" : MessageLookupByLibrary.simpleMessage("Remover"),
    "Remove all other devices" : MessageLookupByLibrary.simpleMessage("Eliminar todos los otros dispositivos"),
    "Remove device" : MessageLookupByLibrary.simpleMessage("Eliminar dispositivo"),
    "Remove exile" : MessageLookupByLibrary.simpleMessage("Eliminar el exilio"),
    "Remove message" : MessageLookupByLibrary.simpleMessage("Remover mensaje"),
    "Render rich message content" : MessageLookupByLibrary.simpleMessage("Renderizar contenido con formato"),
    "Reply" : MessageLookupByLibrary.simpleMessage("Responder"),
    "Request permission" : MessageLookupByLibrary.simpleMessage("Solicitar permiso"),
    "Request to read older messages" : MessageLookupByLibrary.simpleMessage("Solicitar leer mensajes previos"),
    "Revoke all permissions" : MessageLookupByLibrary.simpleMessage("Revocar todos los permisos"),
    "Room has been upgraded" : MessageLookupByLibrary.simpleMessage("La sala ha sido actualizada"),
    "Saturday" : MessageLookupByLibrary.simpleMessage("Sábado"),
    "Search for a chat" : MessageLookupByLibrary.simpleMessage("Buscar un chat"),
    "Seen a long time ago" : MessageLookupByLibrary.simpleMessage("Visto hace mucho tiempo"),
    "Send" : MessageLookupByLibrary.simpleMessage("Enviar"),
    "Send a message" : MessageLookupByLibrary.simpleMessage("Enviar un mensaje"),
    "Send file" : MessageLookupByLibrary.simpleMessage("Enviar un archivo"),
    "Send image" : MessageLookupByLibrary.simpleMessage("Enviar una imagen"),
    "Set a profile picture" : MessageLookupByLibrary.simpleMessage("Asignar una foto de perfil"),
    "Set group description" : MessageLookupByLibrary.simpleMessage("Asignar la descripción del grupo"),
    "Set invitation link" : MessageLookupByLibrary.simpleMessage("Asignar enlace de invitación"),
    "Set status" : MessageLookupByLibrary.simpleMessage("Asignar estado"),
    "Settings" : MessageLookupByLibrary.simpleMessage("Ajustes"),
    "Share" : MessageLookupByLibrary.simpleMessage("Compartir"),
    "Sign up" : MessageLookupByLibrary.simpleMessage("Registrarse"),
    "Source code" : MessageLookupByLibrary.simpleMessage("Código fuente"),
    "Start your first chat :-)" : MessageLookupByLibrary.simpleMessage("Inicia tu primer chat :-)"),
    "Sunday" : MessageLookupByLibrary.simpleMessage("Domingo"),
    "System" : MessageLookupByLibrary.simpleMessage("Sistema"),
    "Tap to show menu" : MessageLookupByLibrary.simpleMessage("Toca para mostrar menú"),
    "The encryption has been corrupted" : MessageLookupByLibrary.simpleMessage("La encripción ha sido corrompida"),
    "This room has been archived." : MessageLookupByLibrary.simpleMessage("Esta sala ha sido archivada"),
    "Thursday" : MessageLookupByLibrary.simpleMessage("Jueves"),
    "Try to send again" : MessageLookupByLibrary.simpleMessage("Intenta enviar de nuevo"),
    "Tuesday" : MessageLookupByLibrary.simpleMessage("Martes"),
    "Unknown device" : MessageLookupByLibrary.simpleMessage("Dispositivo desconocido"),
    "Unknown encryption algorithm" : MessageLookupByLibrary.simpleMessage("Algoritmo de encripción desconocido"),
    "Unmute chat" : MessageLookupByLibrary.simpleMessage("Desilenciar chat"),
    "Use Amoled compatible colors?" : MessageLookupByLibrary.simpleMessage("¿Utilizar colores compatibles con AMOLED?"),
    "Username" : MessageLookupByLibrary.simpleMessage("Nombre de usuario"),
    "Verify" : MessageLookupByLibrary.simpleMessage("Verificar"),
    "Video call" : MessageLookupByLibrary.simpleMessage("Video llamada"),
    "Visibility of the chat history" : MessageLookupByLibrary.simpleMessage("Visibilidad del historial de chat"),
    "Visible for all participants" : MessageLookupByLibrary.simpleMessage("Visible para todos los participantes"),
    "Visible for everyone" : MessageLookupByLibrary.simpleMessage("Visible para todos"),
    "Voice message" : MessageLookupByLibrary.simpleMessage("Mensaje de voz"),
    "Wallpaper" : MessageLookupByLibrary.simpleMessage("Fondo de pantalla"),
    "Wednesday" : MessageLookupByLibrary.simpleMessage("Miércoles"),
    "Welcome to the cutest instant messenger in the matrix network." : MessageLookupByLibrary.simpleMessage("Bienvenido al cliente de mensajería instantánea más tierno en la red Matrix"),
    "Who is allowed to join this group" : MessageLookupByLibrary.simpleMessage("Quién tiene permitido unirse al grupo"),
    "Write a message..." : MessageLookupByLibrary.simpleMessage("Escribe un mensaje…"),
    "Yes" : MessageLookupByLibrary.simpleMessage("Sí"),
    "You" : MessageLookupByLibrary.simpleMessage("Tú"),
    "You are invited to this chat" : MessageLookupByLibrary.simpleMessage("Estás invitado a este chat"),
    "You are no longer participating in this chat" : MessageLookupByLibrary.simpleMessage("Ya no eres participante en este chat"),
    "You cannot invite yourself" : MessageLookupByLibrary.simpleMessage("No te puedes invitar a ti mismo"),
    "You have been banned from this chat" : MessageLookupByLibrary.simpleMessage("Has sido prohibido de este chat"),
    "You won\'t be able to disable the encryption anymore. Are you sure?" : MessageLookupByLibrary.simpleMessage("Ya no podrá desactivar la encripción, ¿estás segurx?"),
    "Your own username" : MessageLookupByLibrary.simpleMessage("Tu propio nombre de usuario"),
    "acceptedTheInvitation" : m0,
    "activatedEndToEndEncryption" : m1,
    "alias" : MessageLookupByLibrary.simpleMessage("apodo"),
    "bannedUser" : m2,
    "byDefaultYouWillBeConnectedTo" : m3,
    "changedTheChatAvatar" : m4,
    "changedTheChatDescriptionTo" : m5,
    "changedTheChatNameTo" : m6,
    "changedTheChatPermissions" : m7,
    "changedTheDisplaynameTo" : m8,
    "changedTheGuestAccessRules" : m9,
    "changedTheGuestAccessRulesTo" : m10,
    "changedTheHistoryVisibility" : m11,
    "changedTheHistoryVisibilityTo" : m12,
    "changedTheJoinRules" : m13,
    "changedTheJoinRulesTo" : m14,
    "changedTheProfileAvatar" : m15,
    "changedTheRoomAliases" : m16,
    "changedTheRoomInvitationLink" : m17,
    "couldNotDecryptMessage" : m18,
    "countParticipants" : m19,
    "createdTheChat" : m20,
    "dateAndTimeOfDay" : m21,
    "dateWithYear" : m22,
    "dateWithoutYear" : m23,
    "emoteExists" : MessageLookupByLibrary.simpleMessage("¡El emoticón ya existe!"),
    "emoteInvalid" : MessageLookupByLibrary.simpleMessage("¡Atajo de emoticón inválido!"),
    "emoteWarnNeedToPick" : MessageLookupByLibrary.simpleMessage("¡Necesita seleccionar un atajo de emoticón y una imagen!"),
    "groupWith" : m24,
    "hasWithdrawnTheInvitationFor" : m25,
    "inviteContactToGroup" : m26,
    "inviteText" : m27,
    "invitedUser" : m28,
    "is typing..." : MessageLookupByLibrary.simpleMessage("está escribiendo..."),
    "joinedTheChat" : m29,
    "kicked" : m30,
    "kickedAndBanned" : m31,
    "lastActiveAgo" : m32,
    "loadCountMoreParticipants" : m33,
    "logInTo" : m34,
    "numberSelected" : m35,
    "ok" : MessageLookupByLibrary.simpleMessage("OK"),
    "play" : m36,
    "redactedAnEvent" : m37,
    "rejectedTheInvitation" : m38,
    "removedBy" : m39,
    "seenByUser" : m40,
    "seenByUserAndCountOthers" : m41,
    "seenByUserAndUser" : m42,
    "sentAFile" : m43,
    "sentAPicture" : m44,
    "sentASticker" : m45,
    "sentAVideo" : m46,
    "sentAnAudio" : m47,
    "sharedTheLocation" : m48,
    "timeOfDay" : m49,
    "title" : MessageLookupByLibrary.simpleMessage("FluffyChat"),
    "unbannedUser" : m50,
    "unknownEvent" : m51,
    "unreadChats" : m52,
    "unreadMessages" : m53,
    "unreadMessagesInChats" : m54,
    "userAndOthersAreTyping" : m55,
    "userAndUserAreTyping" : m56,
    "userIsTyping" : m57,
    "userLeftTheChat" : m58,
    "userSentUnknownEvent" : m59
  };
}
